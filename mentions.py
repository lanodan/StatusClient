#!/usr/bin/python3
# -*- encoding: utf-8 -*-
from html import unescape
from time import sleep
import common
import config


def get_mentions(since_id):
	r = api.request('statuses/mentions_timeline', {'since_id' : since_id})
	for item in r:
		try:
			print(common.f['tweet'] % {'id': item['id'], 'screen_name': item['user']['screen_name'], 'name': item['user']['name'], 'text': unescape(item['text'])})
		except KeyError:
			print(item)
	try:
		return item['id']
	except:
		return since_id

if __name__ == "__main__":
	user = dict()
	username = list(config.credentials['users'])[0]
	user = config.credentials['users'][username]
	user['name'] = username
	username = None
	api = common.api(config.credentials['consumer_key'], config.credentials['consumer_secret'], user['access_token_key'], user['access_token_secret'])
	since_id = '571110938467921921' # Garbage id need to save it to a file maybe json
	while True:
		old_id = since_id
		since_id = get_mentions(since_id)
		if old_id != since_id:
			print('\033[1mSince_id:\033[0m', since_id)

		sleep(60)
