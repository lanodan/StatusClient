#!/usr/bin/python3
# -*- encoding: utf-8 -*-
import sys
import common, config

api = common.api(
	config.credentials['consumer_key'],
	config.credentials['consumer_secret'],
	config.credentials['users'][common.user_id]['access_token_key'],
	config.credentials['users'][common.user_id]['access_token_secret']
)

def favorite(id):
	""" required argument, id """
	api.request('favorites/create', {'id': id})

def retweet(id):
	""" required argument: id """
	api.request('statuses/retweet/:{}'.format(id))

def reply(in_reply_to_status_id, status):
	""" required arguments: in_reply_to_status_id, status """
	api.request('statuses/update', {'in_reply_to_status_id': in_reply_to_status_id, 'status': status})

def delete(id):
	""" required argument: id """
	api.request('statuses/destroy/:{}'.format(id))

def nsfw(status):
	""" required argument: status """
	api.request('statuses/update', {'status': status, 'possibly_sensitive': True})

def upload(file_path, status):
	""" required argument: file_path, status """
	file = open(file_path, 'rb')
	r = api.request('media/upload', None, {'media': file.read()})
	api.request('statuses/update', {'status': status, 'media_ids': r.json()['media_id']})
	file.close()

def upload_nsfw(file_path, status):
	""" required argument: file_path, status """
	file = open(file_path, 'rb')
	r = api.request('media/upload', None, {'media': file.read()})
	api.request('statuses/update', {'status': status, 'media_ids': r.json()['media_id'], 'possibly_sensitive': True})
	file.close()

def follow(target):
	if target.isalpha():
		api.request('friendships/create', {'screen_name': target})
	else:
		api.request('friendships/create', {'screen_name': target})
	
def unfollow(target):
	if target.isalpha():
		api.request('friendships/destroy', {'screen_name': target})
	else:
		api.request('friendships/destroy', {'user_id': target})

def usage():
	pipe = '\033[0m|\033[1m'
	print('Delete: /<\033[1mdel', pipe, 'delete', pipe, 'destroy\033[0m> <\033[1mid\033[0m>')
	print('Favorite & Retweet: /<frt|favrt> <id>')
	print('Favorite: /<f|fav|favorite> <id>')
	print('NSFW: /nsfw <status>')
	print('Reply: /<re|reply> <id> <status>')
	print('Retweet: /<rt|retweet> <id>')
	print('Upload(image): /upload <file_path> <status>')
	print('NSFW Upload: /<nsfw_upload|upload_nsfw> <file_path> <status>')
	print('Reply Upload: /<re_up|re_upload> <id> <file_path> <status>')
	print('Follow: /follow <screen_name|user_id>')
	print('Unfollow: /unfollow <screen_name|user_id>')
	print('To tweet just put it without any command.')
	print('Protip: To send a direct message prepend MS @<name> or M$ @<name>. Lazy dev is lazy.')
	
def parse_command(status):
	if status[0] == '/':
		command = status[1:].split(' ')
		if command[1] is not None:
			if command[0] in ('f', 'fav', 'favorite'):
				favorite(id = command[1])
			elif command[0] in ('rt', 'retweet'):
				retweet(id = command[1])
			elif command[0] in ('frt', 'favrt'):
				favorite(id = command[1])
				retweet(id = command[1])
			elif command[0] in ('re', 'reply'):
				reply(in_reply_to_status_id = command[1], status = status.split(' ', 2)[2])
			elif command[0] in ('del', 'delete', 'destroy'):
				delete(command[1])
			elif command[0] == 'nsfw':
				nsfw(status = command[1])
			elif command[0] == 'upload':
				upload(status = status.split(' ', 2)[2], file_path = command[1])
			elif command[0] == 'upload_nsfw':
				upload(status = status.split(' ', 2)[2], file_path = command[1])
			elif command[0] == 'follow':
				follow(command[1])
			elif command[0] == 'unfollow':
				unfollow(command[1])
			elif command[0] in ('tweet', 'status'):
				print('No command is used, just put it directly')
			else:
				print(status, 'doesn’t exist.')
		else:
			print(status, 'you’ve forgotten arguments.')
	else:
		if len(status) == 0:
			print('Status cannot be empty')
		else:
			print(api.request('statuses/update', {'status': status}))

if __name__ == "__main__":
	while 1:
		prompt = input('>>')
		parse_command(prompt)
