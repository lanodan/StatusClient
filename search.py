#!/usr/bin/python3
# -*- encoding: utf-8 -*-
from TwitterAPI import TwitterAPI, TwitterOAuth, TwitterRestPager

o = TwitterOAuth.read_file('credentials.txt')
api = TwitterAPI(
	o.consumer_key,
	o.consumer_secret,
	o.access_token_key,
	o.access_token_secret)

def search(args):
	try:
		r = api.request('search/tweets', {'q': args[1:]})
		for item in r:
			if 'text' in item:
				print(item['id'], ' <\033[1m', item['user']['screen_name'], '\033[0m> — ', item['user']['name'], sep='')
			else:
				print(item)
		
		print('QUOTA: ', r.get_rest_quota())
	except:
		print(sys.exc_info())

if __name__ == "__main__":
	import sys
	search(sys.argv[1:])
