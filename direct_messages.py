#!/usr/bin/python3
# -*- encoding: utf-8 -*-
from TwitterAPI import TwitterAPI, TwitterOAuth, TwitterRestPager

o = TwitterOAuth.read_file('credentials.txt')
api = TwitterAPI(
	o.consumer_key,
	o.consumer_secret,
	o.access_token_key,
	o.access_token_secret)

def get_direct_messages():
	r = api.request('direct_messages/sent')
	print(r)

if __name__ == "__main_":
	get_direct_messages()
