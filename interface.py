#!/usr/bin/python3
# -*- encoding: utf-8 -*-
import curses

screen = curses.initscr()
screen.clear()

winStream = curses.newwin(0, 0)
winStream.addstr("""Testing windows in curses""")
winStream.refresh()
winStream.getkey()

curses.endwin()
