try:
	from StatusAPI import StatusAPI, StatusOAuth, StatusRestPager
except ImportError:
	from TwitterAPI import TwitterAPI as StatusAPI, TwitterOAuth as StatusOAuth, TwitterRestPager as StatusRestPager

f = {
	'default': ' * \033[1m %s %s %s “\033[3m%s\033[0m” *',
	'favorite': ' * \033[1;33m %s %s %s “\033[3m%s\033[0m” *',
	'retweet': ' * \033[1;32m %s %s %s “\033[3m%s\033[0m” *',
	'follow': ' * \033[1m %s %s %s \033[0m *',
	'delete': ' * \033[1m Deleted: %s(id) \033[0m *',
	'tweet': '%(id)s @\033[1m%(screen_name)s — %(name)s \033[0m\n\t%(text)s'
}

friend_list_garbage = 'Gave me friend list, I think it’s garbage, since I want the timeline'
user_id = 0 #@TODO: Modify this later for multi-user

def api(consumer_key, consumer_secret, access_token_key, access_token_secret):
	return StatusAPI(
		consumer_key,
		consumer_secret,
		access_token_key,
		access_token_secret
	)
